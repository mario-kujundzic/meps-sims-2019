package gui;

import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;

import model.State;

public class GuiNone extends GuiState {

	@Override
	void entry() {
		System.out.println("STANJE - View state");
	}

	@Override
	void exit() {

	}

	@Override
	void handleEvent(MouseEvent e) {
		State collisionState = null;
		// Check if you can find a state
		for (State s : window.app.getStates().values()) {
			if (e.getX() > s.getX() && e.getX() < (s.getX() + s.X_SIZE) && e.getY() > s.getY()
					&& e.getY() < (s.getY() + s.Y_SIZE))
				collisionState = s;
		}
		// If no button was clicked, return
		if (collisionState == null)
			return;
		// Enter the state editing dialog
		if (SwingUtilities.isLeftMouseButton(e)) {
			// TODO - Pozivanje metode za otvaranje i editovanje stanja - Luka
			System.out.println("OPEN - editor");
			new EditWindow(window, collisionState);

		}
		// Delete the selected state
		if (SwingUtilities.isRightMouseButton(e)) {
			window.removeState(collisionState);
		}

	}
}
