package gui;

import java.awt.event.MouseEvent;

public abstract class GuiState {
	protected MainWindow window;
	abstract void entry();
	
	abstract void exit();
	
	abstract void handleEvent(MouseEvent e);
}
