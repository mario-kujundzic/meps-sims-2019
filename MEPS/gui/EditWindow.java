package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import model.EntityName;
import model.Field;
import model.State;
import model.StateField;

public class EditWindow {
	
	// ComboBox for selecting entity type
	protected JComboBox<String> comboBoxEntity;

	// ComboBox for selecting field type
	protected JComboBox<String> comboBoxField;
	
	protected JPanel editPanel = new JPanel();
	
	protected MainWindow mainWindow;
	
	protected JOptionPane jop;
	
	protected JDialog dialog;
	
	private State stateBeingEdited;
	
	private int fieldsCount = 0;
	
	private JTextField displayName;
	
	private JTextField lifecycleTextField;
	
	private HashMap<String, Vector<JCheckBox>> stateFieldsCheckBox = new HashMap<String, Vector<JCheckBox>>();
	
	private JCheckBox initialState = new JCheckBox("Initial State");
	
	private JCheckBox finalState = new JCheckBox("Final State");
	
	private JCheckBox saveEnabled = new JCheckBox("Save Enabled");
	
	private JCheckBox deleteEnabled = new JCheckBox("Delete Enabled");
	
	public EditWindow(MainWindow mw, State s) {
		mainWindow = mw;
		stateBeingEdited = s;
		buildPanels();
		editPanel.setBounds(0, 0, 400, 600);
		JScrollPane scroller = new JScrollPane(editPanel);
		scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroller.setBounds(0, 0, 400, 600);
		
		dialog = new JDialog(mw.frame, "Editor - " +s.getDisplayName(), true);
		dialog.setLocation(mw.frame.getX() + mw.frame.getWidth() - 380, mw.frame.getY());
		dialog.setSize(380, mw.frame.getHeight());
		dialog.add(scroller, BorderLayout.CENTER);
		dialog.setVisible(true);
	};
	
	private void buildPanels() {
		Vector<String> entityNames = new Vector<String>();
		entityNames.add("AccessPermit");
		entityNames.add("SwitchOrder");
		entityNames.add("SwitchRequest");
		
		editPanel.setLayout(new GridBagLayout());
		GridBagConstraints c;
		
		// First row
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.NORTH;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		c.gridy = 1;
		editPanel.add(new JLabel("Display name:"), c);

		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.NORTH;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 1;
		c.gridy = 1;
		displayName = new JTextField(stateBeingEdited.getDisplayName());
		editPanel.add(displayName, c);
		
		// Second row
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.NORTH;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		c.gridy = 2;
		editPanel.add(new JLabel("Entity Id:"), c);

		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.NORTH;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 1;
		c.gridy = 2;
		JTextField outEntityId = new JTextField(Integer.toString(stateBeingEdited.getEntityId()));
		outEntityId.setEditable(false);
		editPanel.add(outEntityId, c);
		// Third row
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.NORTH;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		c.gridy = 3;
		editPanel.add(new JLabel("Lifecycle name:"), c);

		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.NORTH;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 1;
		c.gridy = 3;
		lifecycleTextField = new JTextField(stateBeingEdited.getLifecycleName());
		if(stateBeingEdited.getLifecycleName() == null) lifecycleTextField = new JTextField("Default value");
		editPanel.add(lifecycleTextField, c);
		
		// Fourth row
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		c.gridy = 4;
		editPanel.add(new JLabel("Entity name:"), c);

		c = new GridBagConstraints();
		comboBoxEntity = new JComboBox<String>(entityNames);
		comboBoxEntity.setSelectedIndex(stateBeingEdited.getEntityName().ordinal());
		comboBoxEntity.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				updateFieldBox(mainWindow.app.getAvailableFields().get(comboBoxEntity.getSelectedItem()));
			}
		});
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 1;
		c.gridy = 4;
		editPanel.add(comboBoxEntity, c);
		// Fifth row
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 5;
		initialState.setSelected(stateBeingEdited.isInitState());
		editPanel.add(initialState, c);

		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 5;
		saveEnabled.setSelected(stateBeingEdited.isSaveEnabled());
		editPanel.add(saveEnabled, c);
		// Sixth row
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 6;
		finalState.setSelected(stateBeingEdited.isFinalState());
		editPanel.add(finalState, c);

		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 6;
		deleteEnabled.setSelected(stateBeingEdited.isDeleteEnabled());
		editPanel.add(deleteEnabled, c);
		// Seventh row
		c = new GridBagConstraints();
		comboBoxField = new JComboBox<String>();
		updateFieldBox(mainWindow.app.getAvailableFields().get(comboBoxEntity.getSelectedItem()));
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridy = 7;
		comboBoxField.setPreferredSize(new Dimension(260, 25));
		editPanel.add(comboBoxField, c);
		// Eighth row
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridy = 8;
		JButton addField = new JButton("Add field");
		addField.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// Proveri combo box i kreiraj odgovarajuci field
				System.out.println("Add field bttn - editor");
				String fieldName = (String)comboBoxField.getSelectedItem();
				System.out.println(fieldName);
				// Ako vec postoji taj tip fielda, zabrani
				if (stateFieldsCheckBox.isEmpty() == false) {
					if (stateFieldsCheckBox.containsKey(fieldName)) {
						System.out.println("Field "+ fieldName+" vec postoji");
						return;
					}
				}
				
				Vector<JCheckBox> checkBoxes = new Vector<JCheckBox>();
				addFieldsToEditPanel(fieldName, checkBoxes, false, false, false);
				stateFieldsCheckBox.put(fieldName, checkBoxes);
				System.out.println(fieldsCount);
			}
		});
		editPanel.add(addField, c);
		
		// Save and Cancel Buttons
		JPanel p7 = new JPanel(new GridLayout());
		
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		c.weighty = 1;
		JButton saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener() { 
			
			@Override
			public void actionPerformed(ActionEvent e) { 
			// Save Button Action
				saveEvent(e);
			} 
		} );
		p7.add(saveButton, c);
		
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 0;
		c.weighty = 1;
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() { 
			
			@Override
			public void actionPerformed(ActionEvent e) { 
				System.out.println("CANCEL BTTN - editor");
				exit();
			} 
		} );
		p7.add(cancelButton, c);
		
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridy = 9;
		c.weighty = 1;
		editPanel.add(p7, c);
		editPanel.setBorder(LineBorder.createBlackLineBorder());
		
		for (StateField sf : stateBeingEdited.getFields()) {
			String fieldName = sf.getFieldType().getValue();
			Vector<JCheckBox> checkBoxes = new Vector<JCheckBox>();
			System.out.println("Loading field "+ fieldName);
			addFieldsToEditPanel(fieldName, checkBoxes, sf.isMandatory(), sf.isDenyModify(), sf.isHide());
			stateFieldsCheckBox.put(fieldName, checkBoxes);
		}
		
	}
	
	private void updateFieldBox(Vector<Field> fields) {
		comboBoxField.removeAllItems();
		for (Field f : fields) {
			comboBoxField.addItem(f.getValue());
		}

	}
	private void saveEvent(ActionEvent e) {
		System.out.println("SAVE BTTN - editor");
		stateBeingEdited.setInitState(initialState.isSelected());
		stateBeingEdited.setFinalState(finalState.isSelected());
		stateBeingEdited.setSaveEnabled(saveEnabled.isSelected());
		stateBeingEdited.setDeleteEnabled(deleteEnabled.isSelected());
		stateBeingEdited.setLifecycleName(lifecycleTextField.getText());
		stateBeingEdited.setDisplayName(displayName.getText());
		stateBeingEdited.setEntityName(EntityName.values()[comboBoxEntity.getSelectedIndex()]);
		// If there are new fields save them
		if (stateFieldsCheckBox.isEmpty() == false) {
			for(Map.Entry<String, Vector<JCheckBox>> entry : stateFieldsCheckBox.entrySet()) {
				ArrayList<StateField> postojeciFields = stateBeingEdited.getFields();
				
				// If the field was only modified, modify it
				if(fieldExists(entry.getKey())) {
					for(StateField sf : postojeciFields) {
						if(sf.getFieldType().getValue().equals(entry.getKey())) {
							System.out.println("Saving OLD field " +entry.getKey());
							Vector<JCheckBox> fieldFlags = entry.getValue();
							sf.setHide(fieldFlags.remove(2).isSelected());
							sf.setDenyModify(fieldFlags.remove(1).isSelected());
							sf.setMandatory(fieldFlags.remove(0).isSelected());
							break;
						}
					}
				// Else create new stateField
				}else {
					System.out.println("Saving NEW field " + entry.getKey());
					Vector<JCheckBox> fieldFlags = entry.getValue();
					EntityName fieldType = null;
					switch(entry.getKey()){
					case "AccessPermit":
						fieldType = EntityName.AccessPermit;
					case "SwitchOrder":
						fieldType = EntityName.SwitchOrder;
					case "SwitchRequest":
						fieldType = EntityName.SwitchRequest;
					}
					StateField newSF = new StateField(fieldFlags.remove(1).isSelected(),
							fieldFlags.remove(1).isSelected(),
							fieldFlags.remove(0).isSelected(),
							new Field(fieldType, entry.getKey()));
					stateBeingEdited.addFields(newSF);
				}
			}
			System.out.println();
		}else {
			System.out.println("No fields to save");
		}
		exit();
	}
	
	
	// Adds a field to the fields panel by adding 2 rows to the gridBagLayout of the panel.
	// The first row is the field name, and the second one is 3 flags (checkBoxes)
	private void addFieldsToEditPanel(String fieldName, Vector<JCheckBox> checkBoxes, boolean mand, boolean denyMod, boolean hid) {
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.gridx = 0;
		c.gridy = 10 + fieldsCount*2;
		editPanel.add(new JLabel("Field name:"), c);
		
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 10 + fieldsCount*2;
		c.gridwidth = GridBagConstraints.REMAINDER;
		JTextField out = new JTextField(fieldName);
		out.setEditable(false);
		editPanel.add(out, c);
		
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.gridx = 0;
		c.gridy = 10 + fieldsCount*2 + 1;
		JCheckBox mandatory = new JCheckBox("Mandatory");
		mandatory.setSelected(mand);
		editPanel.add(mandatory);
		
		
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.gridx = 1;
		c.gridy = 10 + fieldsCount*2 + 1;
		JCheckBox denyModify = new JCheckBox("Deny Modify");
		denyModify.setSelected(denyMod);
		editPanel.add(denyModify);
		
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.gridx = 2;
		c.gridy = 10 + fieldsCount*2 + 1;
		JCheckBox hide = new JCheckBox("Hide");
		hide.setSelected(hid);
		editPanel.add(hide);
		
		checkBoxes.addElement(mandatory);
		checkBoxes.addElement(denyModify);
		checkBoxes.addElement(hide);
		
		editPanel.revalidate();
		editPanel.repaint();
		fieldsCount++;
	};
	
	private boolean fieldExists(String fieldKey) {
		for (StateField sf : stateBeingEdited.getFields()) {
			if (sf.getFieldType().getValue().equals(fieldKey))
				return true;
		}
		return false;
	}
	
	private void exit() {
		mainWindow.mainPanel.repaint();
		dialog.dispose();
	}
}
