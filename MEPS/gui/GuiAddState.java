package gui;

import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import model.State;

public class GuiAddState extends GuiState {

	@Override
	void entry() {
		System.out.println("STANJE - Add state");
	}

	@Override
	void exit() {
	}

	@Override
	void handleEvent(MouseEvent e) {
		// Check if you can find overlap
		State collisionState = null;
		for (State s : window.app.getStates().values()) {
			if (s.getX() > e.getX() + s.X_SIZE || e.getX() > s.getX() + s.X_SIZE)
				continue;
			if (s.getY() > e.getY() + s.Y_SIZE || e.getY() > s.getY() + s.Y_SIZE)
				continue;
			collisionState = s;
			break;
		}
		if (collisionState == null && SwingUtilities.isLeftMouseButton(e))
			window.addState(e.getX(), e.getY());
		if (collisionState != null) {
			System.out.println("Already found state at that location: " + collisionState.getDisplayName());
			JOptionPane.showMessageDialog(null, "State found on mouse cursor!", "Error", JOptionPane.ERROR_MESSAGE);
		}
		window.changeState(new GuiNone());
		
	}

}
