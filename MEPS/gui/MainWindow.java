package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.border.LineBorder;

import model.Application;
import model.SaverLoader;
import model.State;
import model.Transition;

public class MainWindow {
	protected Application app;

	protected GuiState editorState;

	protected MouseAdapter mainPanelListener;

	// Main window
	JFrame frame = new JFrame("MEPS");

	// Menu bar
	private JMenuBar menu = new JMenuBar();

	// Toolbar for adding states and transitions
	private JToolBar toolbar = new JToolBar();

	// Main GUI panel to show states and transitions
	protected JPanel mainPanel = new JPanel() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public void paintComponent(java.awt.Graphics g) {
			super.paintComponent(g);
			paintEvent(g);
		};
	};

	// TODO - Pomeriti ovo u novu klasu - Luka
	// Editing panel to add fields to states
	protected JPanel editPanel = new JPanel();

	// ComboBox for selecting entity type
	//protected JComboBox<String> comboBoxEntity;

	// ComboBox for selecting field type
	//protected JComboBox<String> comboBoxField;

	// First state for transition
	protected State firstState;

	public MainWindow() {
		app = new Application();
		editorState = new GuiNone();
		frame.setBounds(100, 100, 1200, 650);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.getContentPane().setLayout(new GridBagLayout());

		buildMenu();

		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridheight = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.LINE_START;
		frame.getContentPane().add(menu, c);

		buildToolbox();

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridheight = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.LINE_START;
		frame.getContentPane().add(toolbar, c);

		buildMainPanel();

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = GridBagConstraints.RELATIVE;
		c.gridheight = GridBagConstraints.REMAINDER;
		c.weighty = 8;
		c.weightx = 1;
		c.anchor = GridBagConstraints.LINE_START;
		c.fill = GridBagConstraints.BOTH;
		frame.getContentPane().add(mainPanel, c);

		frame.setVisible(true);

	}

	private void buildMainPanel() {
		mainPanel.setBorder(LineBorder.createBlackLineBorder());
		mainPanel.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				// Let current state handle the event
				editorState.handleEvent(e);
			}
		});
	}

	protected void paintEvent(Graphics g) {
		// First draw all transitions
		g.setColor(Color.BLACK);
		for (State s : app.getStates().values()) {
			for (Transition t : s.getTransitions()) {
				int x1 = t.getComesFrom().getX() + s.X_SIZE / 2;
				int y1 = t.getComesFrom().getY() + s.Y_SIZE / 2;
				int x2 = t.getLeadsTo().getX() + s.X_SIZE / 2;
				int y2 = t.getLeadsTo().getY() + s.Y_SIZE / 2;
				g.drawLine(x1, y1, x2, y2);
				g.drawString(t.getComesFrom().getEntityId() + "->" + t.getLeadsTo().getEntityId(), (x1 + x2) / 2, (y1 + y2) / 2);
			}
		}
		// Then draw all states so lines don't overlap
		for (State s : app.getStates().values()) {
			g.setColor(Color.LIGHT_GRAY);
			g.fillRect(s.getX(), s.getY(), s.X_SIZE, s.Y_SIZE);
			g.setColor(Color.BLACK);
			g.drawRect(s.getX(), s.getY(), s.X_SIZE, s.Y_SIZE);
			g.setFont(g.getFont().deriveFont((float) 20.0));
			g.drawString(s.getDisplayName(), s.getX(), s.getY() + s.Y_SIZE / 2 + s.Y_SIZE / 8);
		}
	}

	private void buildToolbox() {
		JButton stateButton = new JButton("Add state");
		stateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeState(new GuiAddState());
			}
		});
		toolbar.add(stateButton);
		toolbar.addSeparator();
		JButton transitionButton = new JButton("Add transition");
		transitionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeState(new GuiAddTrans());
			}
		});
		toolbar.add(transitionButton);
		toolbar.setFloatable(false);
	}

	@SuppressWarnings("serial")
	private void buildMenu() {
		JMenu fileMenu = new JMenu("File");
		JMenuItem fileMenuNew = new JMenuItem(new AbstractAction("New") {
			@Override
			public void actionPerformed(ActionEvent e) {//bela
				SaverLoader.newF(app, frame);
			}
		});
		JMenuItem fileMenuSave = new JMenuItem(new AbstractAction("Save") {
			@Override
			public void actionPerformed(ActionEvent e) {
				SaverLoader.save(app, frame);
			}
		});
		JMenuItem fileMenuOpen = new JMenuItem(new AbstractAction("Open") {
			@Override
			public void actionPerformed(ActionEvent e) {
				SaverLoader.open(app, frame);
			}
		});
		JMenuItem fileGenerate = new JMenuItem(new AbstractAction("Generate XML") {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				SaverLoader.generateXML(app, frame);
			}
		});
		
		JMenu editMenu = new JMenu("Edit");
		JMenuItem editMenuCopy = new JMenuItem("Copy");
		JMenuItem editMenuPaste = new JMenuItem("Paste");

		JMenu viewMenu = new JMenu("View");
		JMenuItem viewMenuZoomIn = new JMenuItem("Zoom-in");
		JMenuItem viewMenuZoomOut = new JMenuItem("Zoom-out");

		menu.add(fileMenu);
		menu.add(editMenu);
		menu.add(viewMenu);

		fileMenu.add(fileMenuNew);
		fileMenu.add(fileMenuOpen);
		fileMenu.add(fileMenuSave);
		fileMenu.add(fileGenerate);

		editMenu.add(editMenuCopy);
		editMenu.add(editMenuPaste);

		viewMenu.add(viewMenuZoomIn);
		viewMenu.add(viewMenuZoomOut);
	}

	protected void changeState(GuiState newState) {
		editorState.exit();
		editorState = newState;
		editorState.window = this;
		editorState.entry();
	}

	protected void addState(int x, int y) {
		app.addState(x, y);
		mainPanel.repaint();
		System.out.println("State added!");
	}

	protected void addTransition(State firstState, State secondState) {
		app.addTransition(firstState, secondState);
		mainPanel.repaint();
		System.out.println("Transition added!");
	}

	public void removeState(State collisionState) {
		app.deleteState(collisionState);
		mainPanel.repaint();
		System.out.println("State deleted!");
	}

}
