package gui;

import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import model.State;
import model.Transition;

public class GuiAddTrans extends GuiState {

	@Override
	void entry() {
		System.out.println("STANJE - Add transition");

	}

	@Override
	void exit() {
		window.firstState = null;
	}

	@Override
	void handleEvent(MouseEvent e) {
		// Check if a state was clicked
		State collisionState = null;
		for (State s : window.app.getStates().values()) {
			if (e.getX() > s.getX() && e.getX() < (s.getX() + s.X_SIZE) && e.getY() > s.getY()
					&& e.getY() < (s.getY() + s.Y_SIZE))
				collisionState = s;
		}
		// If RMB clicked, cancel
		if (SwingUtilities.isRightMouseButton(e)) {
			window.changeState(new GuiNone());
			return;
		}
		// If no state is selected, wait for next click
		if (collisionState == null)
			return;
		// If first state to be clicked, set it as so
		if (window.firstState == null) {
			System.out.println("First state for transition: " + collisionState.getDisplayName());
			window.firstState = collisionState;
		} else {
			// If the same state is selected, error
			if (window.firstState == collisionState) {
				System.out.println("Same state clicked, cannot create transition");
				JOptionPane.showMessageDialog(null, "Cannot create transition with self!", "Error", JOptionPane.ERROR_MESSAGE);
			}
			// If a new state is selected, make transition
			else {
				boolean inverseExists = false;
				for (Transition t : collisionState.getTransitions()) {
					if (t.getLeadsTo().getEntityId() == window.firstState.getEntityId())
						inverseExists = true;
				}
				if (!inverseExists) {
					System.out.println("Second state for transition: " + collisionState.getDisplayName());
					window.addTransition(window.firstState, collisionState);					
				} else {
					System.out.println("Cannot create transition because inverse transition exists");
					JOptionPane.showMessageDialog(null, "Inverse transition already exists!", "Error", JOptionPane.ERROR_MESSAGE);
				
				}
			}
			window.changeState(new GuiNone());
		}
	}

}
