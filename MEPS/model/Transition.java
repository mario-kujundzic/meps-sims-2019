package model;

public class Transition {
	private EntityName entityName;

	private State comesFrom;

	private State leadsTo;

	private int entityId;
	
	public Transition() {}
	
	public Transition(State s1, State s2, EntityName en, int id) {
		this();
		this.comesFrom = s1;
		this.leadsTo = s2;
		this.entityName = en;
		this.entityId = id;
	}
	
	public EntityName getEntityName() {
		return entityName;
	}

	public void setEntityName(EntityName entityName) {
		this.entityName = entityName;
	}

	public State getComesFrom() {
		return comesFrom;
	}

	public void setComesFrom(State comesFrom) {
		this.comesFrom = comesFrom;
	}

	public State getLeadsTo() {
		return leadsTo;
	}

	public void setLeadsTo(State leadsTo) {
		this.leadsTo = leadsTo;
	}

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

}