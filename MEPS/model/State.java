package model;

import java.util.*;

public class State {

	private EntityName entityName;

	private String displayName;

	private int entityId;

	private String lifecycleName;

	private int x;

	private int y;

	final public int X_SIZE = 100;

	final public int Y_SIZE = 50;

	private ArrayList<Transition> transitions;

	private ArrayList<StateField> fields;

	private boolean initState;
	
	private boolean finalState;
	
	private boolean saveEnabled;
	
	private boolean deleteEnabled;
	
	public State() {
		transitions = new ArrayList<Transition>();
		fields = new ArrayList<StateField>();
		entityName = EntityName.AccessPermit;
	}
	
	public State(String displayName, String lifecycleName, int entityId, int x, int y) {
		this();
		this.displayName = displayName;
		this.lifecycleName = lifecycleName;
		this.entityId = entityId;
		this.x = x;
		this.y = y;
	}
	
	public ArrayList<Transition> getTransitions() {
		if (transitions == null)
			transitions = new java.util.ArrayList<Transition>();
		return transitions;
	}

	public Iterator<Transition> getIteratorTransitions() {
		if (transitions == null)
			transitions = new java.util.ArrayList<Transition>();
		return transitions.iterator();
	}

	public void addTransitions(Transition newTransition) {
		if (newTransition == null)
			return;
		if (this.transitions == null)
			this.transitions = new java.util.ArrayList<Transition>();
		if (!this.transitions.contains(newTransition))
			this.transitions.add(newTransition);
	}

	public void removeAllTransitions() {
		if (transitions != null)
			transitions.clear();
	}

	public ArrayList<StateField> getFields() {
		if (fields == null)
			fields = new java.util.ArrayList<StateField>();
		return fields;
	}

	public void addFields(StateField newStateField) {
		if (newStateField == null)
			return;
		if (this.fields == null)
			this.fields = new java.util.ArrayList<StateField>();
		if (!this.fields.contains(newStateField))
			this.fields.add(newStateField);
	}

	public void removeFields(StateField oldStateField) {
		if (oldStateField == null)
			return;
		if (this.fields != null)
			if (this.fields.contains(oldStateField))
				this.fields.remove(oldStateField);
	}

	/** @pdGenerated default removeAll */
	public void removeAllFields() {
		if (fields != null)
			fields.clear();
	}

	public EntityName getEntityName() {
		return entityName;
	}

	public void setEntityName(EntityName entityName) {
		this.entityName = entityName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

	public String getLifecycleName() {
		return lifecycleName;
	}

	public void setLifecycleName(String lifecycleName) {
		this.lifecycleName = lifecycleName;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean isInitState() {
		return initState;
	}

	public void setInitState(boolean initState) {
		this.initState = initState;
	}

	public boolean isFinalState() {
		return finalState;
	}

	public void setFinalState(boolean finalState) {
		this.finalState = finalState;
	}

	public boolean isSaveEnabled() {
		return saveEnabled;
	}

	public void setSaveEnabled(boolean saveEnabled) {
		this.saveEnabled = saveEnabled;
	}

	public boolean isDeleteEnabled() {
		return deleteEnabled;
	}

	public void setDeleteEnabled(boolean deleteEnabled) {
		this.deleteEnabled = deleteEnabled;
	}

	public void setTransitions(ArrayList<Transition> transitions) {
		this.transitions = transitions;
	}

	public void setFields(ArrayList<StateField> fields) {
		this.fields = fields;
	}

}