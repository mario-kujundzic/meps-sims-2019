package model;

public class StateField {
	private boolean denyModify;
	
	private boolean hide;
	
	private boolean mandatory;

	public Field fieldType;
	
	public StateField() {
		fieldType = null;
		denyModify = false;
		hide = false;
		mandatory = false;
	}
	
	public StateField(boolean denyM, boolean h, boolean mand, Field f) {
		this();
		denyModify = denyM;
		hide = h;
		mandatory = mand;
		fieldType = f;
	}

	public boolean isDenyModify() {
		return denyModify;
	}

	public void setDenyModify(boolean denyModify) {
		this.denyModify = denyModify;
	}

	public boolean isHide() {
		return hide;
	}

	public void setHide(boolean hide) {
		this.hide = hide;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public Field getFieldType() {
		return fieldType;
	}

	public void setFieldType(Field fieldType) {
		this.fieldType = fieldType;
	}

}