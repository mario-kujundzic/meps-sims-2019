package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import gui.MainWindow;

public class Application {

	private HashMap<String, Vector<Field>> availableFields;

	private ArrayList<Transition> transitions;

	private HashMap<Integer, State> states;

	private int stateCounter;

	private int transitionCounter;

	public Application() {
		setStateCounter(1);
		setTransitionCounter(1);
		availableFields = new HashMap<String, Vector<Field>>();
		states = new HashMap<Integer, State>();
		transitions = new ArrayList<Transition>();
		loadFields();
	}

	public void addState(int x, int y) {
		State st = new State("State " + stateCounter, "Lifecycle " + stateCounter, stateCounter, x, y);
		stateCounter++;
		states.put(st.getEntityId(), st);
		
	}

	public void deleteState(State delState) {
		for (State s : states.values()) {
			if (s.getEntityId() == delState.getEntityId())
				continue;
			Iterator<Transition> it = s.getIteratorTransitions();
			while (it.hasNext()) {
				if (it.next().getLeadsTo().getEntityId() == delState.getEntityId()) {
					it.remove();
				}
			}
		}
		delState.removeAllTransitions();
		states.remove(delState.getEntityId());
	}

	public void addTransition(State firstState, State secondState) {
		Transition t = new Transition(firstState, secondState, firstState.getEntityName(), transitionCounter);
		transitionCounter++;
		firstState.addTransitions(t);
		transitions.add(t);
	}

	public void loadFields() {
		File f = new File("fields.txt");
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
			String line;
			while ((line = in.readLine()) != null) {
				String[] tokens = line.split("\\|");
				EntityName fieldType = null;
				String fieldTypeString = tokens[0];
				switch (fieldTypeString) {
				case "AccessPermit":
					fieldType = EntityName.AccessPermit;
				case "SwitchOrder":
					fieldType = EntityName.SwitchOrder;
				case "SwitchRequest":
					fieldType = EntityName.SwitchRequest;
				}
				if (availableFields.get(fieldTypeString) == null)
					availableFields.put(fieldTypeString, new Vector<Field>());
				for (int i = 1; i != tokens.length; i++) {
					Field fld = new Field(fieldType, tokens[i]);
					availableFields.get(fieldTypeString).add(fld);
				}
			}
			in.close();
		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Transition> getTransitions() {
		return transitions;
	}

	public void setTransitions(ArrayList<Transition> transitions) {
		this.transitions = transitions;
	}

	public HashMap<Integer, State> getStates() {
		return states;
	}

	public void setStates(HashMap<Integer, State> states) {
		this.states = states;
	}

	public HashMap<String, Vector<Field>> getAvailableFields() {
		return availableFields;
	}

	public void setAvailableFields(HashMap<String, Vector<Field>> availableFields) {
		this.availableFields = availableFields;
	}

	public void removeAllAvailableFields() {
		if (availableFields != null)
			availableFields.clear();
	}

	public int getStateCounter() {
		return stateCounter;
	}

	public void setStateCounter(int stateCounter) {
		this.stateCounter = stateCounter;
	}

	public int getTransitionCounter() {
		return transitionCounter;
	}

	public void setTransitionCounter(int transitionCounter) {
		this.transitionCounter = transitionCounter;
	}

	public static void main(String[] args) {
		new MainWindow();
	}

}
