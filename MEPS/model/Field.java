package model;

public class Field {
	private EntityName entity;

	private String value;
	
	public Field() {
	}

	public Field(EntityName entity, String value) {
		super();
		this.setEntity(entity);
		this.setValue(value);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public EntityName getEntity() {
		return entity;
	}

	public void setEntity(EntityName entity) {
		this.entity = entity;
	}

}