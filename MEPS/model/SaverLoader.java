package model;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class SaverLoader {
	static private String fileName = "";
	static private String xmlName = "";
	
	public static void save(Application app, JFrame wind) {
		if(fileName == "") {
			if(!choose(wind, "Save", "file")) return;
		}
		try(BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))){
			for(State s:app.getStates().values()) {
				saveState(s, bw);//letters on beginning signifying state or transition
			}
			
			for(Transition t:app.getTransitions()) {
				saveTransition(t, bw);
			}
			bw.write("a" + "|" + app.getStateCounter() + "|" + app.getTransitionCounter());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void open(Application app, JFrame wind) { // if there is no warning
		if(!app.getStates().isEmpty()) {
			int result = JOptionPane.showConfirmDialog((Component) null, "All unsaved changes will be lost!",
			        "alert", JOptionPane.OK_CANCEL_OPTION);
			if(result == JOptionPane.CANCEL_OPTION || result == JOptionPane.CLOSED_OPTION) return ;
		}
		if(!choose(wind, "Open", "file")) return;
		try(BufferedReader br = new BufferedReader(new FileReader(fileName))){
			String line = br.readLine();
			while(line != null) {
				processLine(app, line);
				line = br.readLine();
			}
			wind.repaint();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void newF(Application app, JFrame wind) {
		if(!app.getStates().isEmpty()) {
			int result = JOptionPane.showConfirmDialog((Component) null, "All unsaved changes will be lost!",
			        "alert", JOptionPane.OK_CANCEL_OPTION);
			if(result == JOptionPane.CANCEL_OPTION || result == JOptionPane.CLOSED_OPTION) return ;
		}
		fileName = "";
		app.setStates(new HashMap<>());
		app.setTransitions(new ArrayList<>());
		app.setTransitionCounter(1);
		app.setStateCounter(1);
		wind.repaint();
	}
	
	public static void generateXML(Application app, JFrame window) {
		if(!choose(window, "Save", "xml")) return;
		// ovo ne ovako, moras resisti neke probleme
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
       		Document doc = dBuilder.newDocument();
	         
	        // Data
	        Element rootElement = doc.createElement("Data");
	        doc.appendChild(rootElement);
	        // Data - Transition
	        Element transitionElement = doc.createElement("Transition");
	        rootElement.appendChild(transitionElement);
	        // Every element is now Data - Transition
	        for (Transition t : app.getTransitions()) {
	        	// Connection
	        	Element connectionEl = doc.createElement("Connection");
	        	connectionEl.setAttribute("entityId", t.getEntityId()+"");
	        	        	
	        	// Connection - Property - LIFECYCLE_NAME
	        	Element lifecycleEl = doc.createElement("Property");
	        	lifecycleEl.setAttribute("mode", "LIFECYCLE_NAME");
	        	// Connection - Property - Value
	        	Element valueElement = doc.createElement("Value");
	        	String lifecycleName = t.getComesFrom().getLifecycleName() + " to " +
	        							t.getLeadsTo().getLifecycleName();
	        	
	        	valueElement.appendChild(doc.createTextNode(lifecycleName));     	
	        	lifecycleEl.appendChild(valueElement);
	        	connectionEl.appendChild(lifecycleEl);

	        	// Connection - Property - ENTITY_NAME
	        	Element entityEl = doc.createElement("Property");
	        	entityEl.setAttribute("model", "ENTITY_NAME");
	        	
	        	valueElement = doc.createElement("EnumValue");
				EntityName entityName = t.getComesFrom().getEntityName();
				valueElement.appendChild(doc.createTextNode(entityName.toString() + ""));
				entityEl.appendChild(valueElement);
				connectionEl.appendChild(entityEl);
				
				// Connection - Property - TRANSITION_ON_SUCCEED
				Element transitionS = doc.createElement("Property");
				transitionS.setAttribute("model", "TRANSITION_ON_SUCEED");
				
				valueElement = doc.createElement("Value");
				valueElement.setAttribute("name", "state");
				valueElement.setAttribute("entityId", t.getLeadsTo().getEntityId()+"");
				
				transitionS.appendChild(valueElement);
				connectionEl.appendChild(transitionS);
				
				// Connection - Property - TRANSITION_ON_FAIL
				Element transitionF = doc.createElement("Property");
				transitionF.setAttribute("model", "TRANSITION_ON_FAIL");
				
				valueElement = doc.createElement("Value");
				valueElement.setAttribute("name", "state");
				valueElement.setAttribute("entityId", t.getComesFrom().getEntityId() + "");
				
				transitionF.appendChild(valueElement);
				connectionEl.appendChild(transitionF);
				
	        	transitionElement.appendChild(connectionEl);
	        }
	        
	        // Data - State
	        Element stateElement = doc.createElement("State");
	        rootElement.appendChild(stateElement);
	        
	        for (State s : app.getStates().values())
	        {
	        	Element connectionEl = doc.createElement("Connection");
	        	connectionEl.setAttribute("entityId", s.getEntityId() + "");
	        	
	        	// LIFECYCLE_NAME
	        	Element lifecycleEl = doc.createElement("Property");
	        	lifecycleEl.setAttribute("model", "LIFECYCLE_NAME");
	        	
	        	Element valueElement = doc.createElement("Value");
	        	String lifecycleName = s.getLifecycleName();
	        	valueElement.appendChild(doc.createTextNode(lifecycleName));     	
	        	lifecycleEl.appendChild(valueElement);
	        	connectionEl.appendChild(lifecycleEl);
	        	
	        	// DISPLAY_NAME
	        	Element displayEl = doc.createElement("Property");
	        	displayEl.setAttribute("model", "DISPLAY_NAME");
	        	
	        	valueElement = doc.createElement("Value");
	        	String displayName = s.getDisplayName();
	        	valueElement.appendChild(doc.createTextNode(displayName));
	        	
	        	displayEl.appendChild(valueElement);
	        	connectionEl.appendChild(displayEl);
	        	
	        	// ENTITY_NAME
	        	Element entityEl = doc.createElement("Property");
	        	entityEl.setAttribute("model", "ENTITY_NAME");
	        	
	        	valueElement = doc.createElement("EnumValue");
				EntityName entityName = s.getEntityName();
				valueElement.appendChild(doc.createTextNode(entityName.toString() + ""));
				entityEl.appendChild(valueElement);
				connectionEl.appendChild(entityEl);
	        	
	        	// STATE_SEMANTIC
	        	Element semanticEl = doc.createElement("Property");
	        	semanticEl.setAttribute("model", "STATE_SEMANTIC");
	        	
	        	if (s.isInitState())
	        	{
	        		valueElement = doc.createElement("EnumValue");
	        		valueElement.appendChild(doc.createTextNode("Init"));
	        		semanticEl.appendChild(valueElement);
	        	}
	        	if (s.isSaveEnabled())
	        	{
	        		valueElement = doc.createElement("EnumValue");
	        		valueElement.appendChild(doc.createTextNode("SaveEnabled"));
	        		semanticEl.appendChild(valueElement);
	        	}
	        	if (s.isDeleteEnabled())
	        	{
	        		valueElement = doc.createElement("EnumValue");
	        		valueElement.appendChild(doc.createTextNode("DeleteEnabled"));
	        		semanticEl.appendChild(valueElement);
	        	}
	        	connectionEl.appendChild(semanticEl);
	        	
	        	// STATE_TRANSITIONS
	        	Element transitionEl = doc.createElement("Property");
	        	transitionEl.setAttribute("model", "STATE_TRANSITIONS");
	        	
	        	for (Transition t : s.getTransitions())
	        	{
	        		valueElement = doc.createElement("Value");
	        		valueElement.setAttribute("entityId", t.getEntityId()+"");
					transitionEl.appendChild(valueElement);
	        	}
	        	connectionEl.appendChild(transitionEl);
	        	
	        	// STATE_DENY_MODIFYING_FIELDS
	        	Element stateDenyEl = doc.createElement("Property");
	        	stateDenyEl.setAttribute("model", "STATE_DENY_MODIFYING_FIELDS");
	        	
	        	for (StateField f : s.getFields())
	        	{
	        		if (f.isDenyModify())
	        		{
	        			valueElement = doc.createElement("EnumValue");
	        			valueElement.appendChild(doc.createTextNode(f.getFieldType().getValue()));
		        		stateDenyEl.appendChild(valueElement);
	        		}
	        	}
	        	connectionEl.appendChild(stateDenyEl);
	        	
	        	// STATE_HIDE_FIELDS
	        	Element stateHideEl = doc.createElement("Property");
	        	stateHideEl.setAttribute("model", "STATE_HIDE_FIELDS");
	        	
	        	for (StateField f : s.getFields())
	        	{
	        		if (f.isHide())
	        		{
	        			valueElement = doc.createElement("EnumValue");
	        			valueElement.appendChild(doc.createTextNode(f.getFieldType().getValue()));
		        		stateHideEl.appendChild(valueElement);
	        		}
	        	}
	        	connectionEl.appendChild(stateHideEl);
	        	
	        	
	        	// STATE_MANDATORY_FIELDS
	        	Element stateMandatoryEl = doc.createElement("Property");
	        	stateMandatoryEl.setAttribute("model", "STATE_MANDATORY_FIELDS");
	        	
	        	for (StateField f : s.getFields())
	        	{
	        		if (f.isMandatory())
	        		{
	        			valueElement = doc.createElement("EnumValue");
	        			valueElement.appendChild(doc.createTextNode(f.getFieldType().getValue()));
		        		stateMandatoryEl.appendChild(valueElement);
	        		}
	        	}
	        	connectionEl.appendChild(stateMandatoryEl);
	        	
	        	stateElement.appendChild(connectionEl);
	        }
	        
	        TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        Transformer transformer = transformerFactory.newTransformer();
	        DOMSource source = new DOMSource(doc);
	        StreamResult result = new StreamResult(new File(xmlName + ".xml"));
	        transformer.transform(source, result);
	        
	        System.out.println("XML generation successful!");
	        
		} catch (Exception e) {
	         e.printStackTrace();
	      }
		
	}
	
	private static void saveState(State s, BufferedWriter bw) throws IOException {
		StringBuilder sb = new StringBuilder("s|");
		sb.append(s.getEntityName() + "|");
		sb.append(s.getDisplayName() + "|");
		sb.append(s.getEntityId() + "|");
		sb.append(s.getLifecycleName() + "|");
		sb.append(s.isInitState() + "|");
		sb.append(s.isFinalState() + "|");
		sb.append(s.isSaveEnabled() + "|");
		sb.append(s.isDeleteEnabled() + "|");
		sb.append(s.getX() + "|");
		sb.append(s.getY() + "|");
		sb.append(constructStateFields(s));
		sb.append("\n");
		bw.write(sb.toString());
	}
	
	private static String constructStateFields(State s) {
		ArrayList<StateField> arr = s.getFields();
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < arr.size(); ++i) {
			if(i != 0) sb.append(";");
			StateField sf = arr.get(i);
			sb.append(sf.isDenyModify() + ":");
			sb.append(sf.isHide() + ":");
			sb.append(sf.isMandatory() + ":");
			sb.append(sf.getFieldType().getEntity() + ":");
			sb.append(sf.getFieldType().getValue());
		}
		return sb.toString();
	}
	
	private static void saveTransition(Transition t, BufferedWriter bw) throws IOException {
		StringBuilder sb = new StringBuilder("t" + "|");
		sb.append(t.getEntityName() + "|");
		sb.append(t.getEntityId() + "|");
		sb.append(t.getComesFrom().getEntityId() + "|");
		sb.append(t.getLeadsTo().getEntityId());
		sb.append("\n");
		bw.write(sb.toString());
	}
	
	
	private static void processLine(Application app, String line) {
		if(line.charAt(0) == 's') {
			State s = constructState(line);
			app.getStates().put(s.getEntityId(), s);
		}
		else if(line.charAt(0) == 't') {
			Transition t = constructTransition(app, line);
			app.getTransitions().add(t);
		}
		else if(line.charAt(0) == 'a'){
			String[] tokens = line.split("\\|");
			app.setStateCounter(Integer.parseInt(tokens[1]));
			app.setTransitionCounter(Integer.parseInt(tokens[2]));
		}
	}
	
	private static State constructState(String line) {
		String[] tokens = line.split("\\|");
		State s = new State();
		s.setEntityName(EntityName.valueOf(tokens[1]));
		s.setDisplayName(tokens[2]);
		s.setEntityId(Integer.parseInt(tokens[3]));
		s.setLifecycleName(tokens[4]);
		s.setInitState(Boolean.parseBoolean(tokens[5]));
		s.setFinalState(Boolean.parseBoolean(tokens[6]));
		s.setSaveEnabled(Boolean.parseBoolean(tokens[7]));
		s.setDeleteEnabled(Boolean.parseBoolean(tokens[8]));
		s.setX(Integer.parseInt(tokens[9]));
		s.setY(Integer.parseInt(tokens[10]));
		if(tokens.length > 11)
			s.setFields(constructStateFields(tokens[11]));
		return s;
	}
	
	private static ArrayList<StateField> constructStateFields(String line){
		String[] tokens = line.split(";");
		ArrayList<StateField> returnArray = new ArrayList<>();
		for(String token:tokens) {
			String[] smallTokens = token.split(":");
			EntityName e = smallTokens[3].equals("null") ? null:EntityName.valueOf(smallTokens[3]);
			returnArray.add(new StateField(Boolean.parseBoolean(smallTokens[0]), 
							Boolean.parseBoolean(smallTokens[1]),
							Boolean.parseBoolean(smallTokens[2]),
							new Field(e, smallTokens[4])));
		}
		return returnArray;
	}
	
	private static Transition constructTransition(Application app, String line) {
		String[] tokens = line.split("\\|");
		Transition t = new Transition();
		t.setEntityName(EntityName.valueOf(tokens[1]));
		t.setEntityId(Integer.parseInt(tokens[2]));
		t.setComesFrom(app.getStates().get(Integer.parseInt(tokens[3])));
		t.setLeadsTo(app.getStates().get(Integer.parseInt(tokens[4])));
		t.getComesFrom().getTransitions().add(t);
		return t;
	}
	
	private static boolean choose(JFrame wind, String title, String path) {
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle(title);
		int res = JFileChooser.CANCEL_OPTION;
		if(title == "Open") {
			res = fc.showOpenDialog(wind);
		}
		else if(title == "Save") {
			res = fc.showSaveDialog(wind);
		}
		if(res == JFileChooser.CANCEL_OPTION)	return false;
		if (path == "file")
			fileName = fc.getSelectedFile().getPath();
		if (path == "xml")
			xmlName = fc.getSelectedFile().getPath();
		return true;
	}
}
